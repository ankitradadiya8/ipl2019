package android.button;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.Button;

import com.lexcorp.crickipl2019.util.Constants;

public class ButtonSFTextRegular extends Button {

    public ButtonSFTextRegular(Context context) {
        super(context);
        applyCustomFont();
    }

    public ButtonSFTextRegular(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        applyCustomFont();
    }

    public ButtonSFTextRegular(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        applyCustomFont();
    }

    private void applyCustomFont() {
        if (!TextUtils.isEmpty(Constants.TEXT_REGULAR)) {
            Typeface tf = Typeface.createFromAsset(getContext().getAssets(), Constants.TEXT_REGULAR);
            setTypeface(tf);
        }
    }
}
