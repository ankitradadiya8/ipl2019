package com.lexcorp.crickipl2019.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.textview.TextViewSFTextRegular;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.lexcorp.crickipl2019.R;
import com.lexcorp.crickipl2019.model.MenuItem;
import com.lexcorp.crickipl2019.ui.BaseActivity;
import com.lexcorp.crickipl2019.ui.TeamsActivity;
import com.lexcorp.crickipl2019.ui.WebViewActivity;
import com.lexcorp.crickipl2019.util.Constants;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.MyViewHolder> {

    private Context mContext;
    private List<MenuItem> mData;

    public CategoryAdapter(Context mContext, ArrayList<MenuItem> arrayList) {
        this.mContext = mContext;
        this.mData = arrayList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.list_category, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        holder.tvTitle.setText(mData.get(position).title);
        Glide.with(mContext).load(mData.get(position).resImage).into(holder.imgLogo);

        holder.imgLogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!((BaseActivity) mContext).isNetworkConnected())
                    return;

                if (position == 0) {
                    redirectView(Constants.MATCHES);
                } else if (position == 1) {
                    mContext.startActivity(new Intent(mContext, TeamsActivity.class));
                } else if (position == 2) {
                    redirectView(Constants.TABLE);
                } else if (position == 3) {
                    redirectView(Constants.NEWS);
                } else if (position == 4) {
                    redirectView(Constants.MATCHES);
                } else if (position == 5) {
                    redirectView(Constants.STATES);
                }
            }
        });
    }

    private void redirectView(String value) {
        Intent i = new Intent(mContext, WebViewActivity.class);
        i.putExtra(Constants.SCREEN_NAME, value);
        mContext.startActivity(i);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_logo)
        ImageView imgLogo;
        @BindView(R.id.tv_title)
        TextViewSFTextRegular tvTitle;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}