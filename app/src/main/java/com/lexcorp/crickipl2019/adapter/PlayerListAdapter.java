package com.lexcorp.crickipl2019.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.textview.TextViewSFTextMedium;
import android.textview.TextViewSFTextRegular;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.lexcorp.crickipl2019.R;
import com.lexcorp.crickipl2019.model.Auction;
import com.lexcorp.crickipl2019.ui.PlayerListActivity;
import com.lexcorp.crickipl2019.util.Constants;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PlayerListAdapter extends RecyclerView.Adapter<PlayerListAdapter.MyViewHolder> {

    private Context mContext;
    private List<Auction.Results> mData;

    public PlayerListAdapter(Context mContext, ArrayList<Auction.Results> arrayList) {
        this.mContext = mContext;
        this.mData = arrayList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_player_list, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        Auction.Results item = mData.get(position);
        holder.playername.setText(item.knownAs);
        holder.role.setText(item.role);
        String format = NumberFormat.getCurrencyInstance(new Locale("en", "IN"))
                .format((long) (Integer.parseInt(item.soldPrice) / 100000));
        holder.sold.setText(format.substring(0, format.length() - 3) + " L");
        if (item.overseas.equals("1")) {
            holder.overseas.setImageResource(R.drawable.ic_airplanemode_active_black_24dp);
            holder.overseas.setRotation(45.0f);
            format = "Overseas";
        } else {
            holder.overseas.setImageResource(R.drawable.ic_home_black_24dp);
            holder.overseas.setRotation(0.0f);
            format = "Indian";
        }

        if (item.retained.equals("0")) {
            holder.imgRetained.setImageResource(R.drawable.ic_brightness_red_black_24dp);
            holder.retained.setText("New Player");
        } else {
            holder.imgRetained.setImageResource(R.drawable.ic_brightness_green_black_24dp);
            holder.retained.setText("Retained");
        }

        final String finalFormat = format;
        holder.overseas.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Toast.makeText(mContext, finalFormat, Toast.LENGTH_SHORT).show();
            }
        });

        holder.card.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
//                view = new Intent(PlayerListAdapter.this.context, PlayerInfo.class);
//                view.putExtra("playerid", PlayerListAdapter.this.jsonread(jSONObject, "id"));
//                view.putExtra("playername", PlayerListAdapter.this.jsonread(jSONObject, "known_as"));
//                view.putExtra("team", PlayerListAdapter.this.team);
//                String str = ImagesContract.URL;
//                StringBuilder stringBuilder = new StringBuilder();
//                stringBuilder.append("http://crickexer.com/ipl2019/playerinfo2.php?p=");
//                stringBuilder.append(PlayerListAdapter.this.jsonread(jSONObject, "id"));
//                view.putExtra(str, stringBuilder.toString());
//                ((playerList) PlayerListAdapter.this.context).intentStart(view);
            }
        });
    }

    private void redirectView(String value) {
        Intent i = new Intent(mContext, PlayerListActivity.class);
        i.putExtra(Constants.TEAM_NAME, value);
        mContext.startActivity(i);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.retained)
        TextViewSFTextRegular retained;
        @BindView(R.id.img_retained)
        ImageView imgRetained;
        @BindView(R.id.playername)
        TextViewSFTextMedium playername;
        @BindView(R.id.sold)
        TextViewSFTextRegular sold;
        @BindView(R.id.role)
        TextViewSFTextRegular role;
        @BindView(R.id.overseas)
        ImageView overseas;
        @BindView(R.id.bottom_line)
        TextView bottomLine;
        @BindView(R.id.card)
        LinearLayout card;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}