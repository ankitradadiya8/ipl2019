package com.lexcorp.crickipl2019.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.textview.TextViewSFTextRegular;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.lexcorp.crickipl2019.R;
import com.lexcorp.crickipl2019.model.MenuItem;
import com.lexcorp.crickipl2019.ui.BaseActivity;
import com.lexcorp.crickipl2019.ui.PlayerListActivity;
import com.lexcorp.crickipl2019.ui.TeamsActivity;
import com.lexcorp.crickipl2019.ui.WebViewActivity;
import com.lexcorp.crickipl2019.util.Constants;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TeamsAdapter extends RecyclerView.Adapter<TeamsAdapter.MyViewHolder> {

    private Context mContext;
    private List<MenuItem> mData;

    public TeamsAdapter(Context mContext, ArrayList<MenuItem> arrayList) {
        this.mContext = mContext;
        this.mData = arrayList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.list_category, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        holder.tvTitle.setText(mData.get(position).title);
        Glide.with(mContext).load(mData.get(position).resImage).into(holder.imgLogo);

        holder.imgLogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!((BaseActivity) mContext).isNetworkConnected())
                    return;

                redirectView(mData.get(position).title);
            }
        });
    }

    private void redirectView(String value) {
        Intent i = new Intent(mContext, PlayerListActivity.class);
        i.putExtra(Constants.TEAM_NAME, value);
        mContext.startActivity(i);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_logo)
        ImageView imgLogo;
        @BindView(R.id.tv_title)
        TextViewSFTextRegular tvTitle;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}