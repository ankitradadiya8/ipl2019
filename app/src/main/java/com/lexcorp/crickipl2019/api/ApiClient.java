package com.lexcorp.crickipl2019.api;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.lexcorp.crickipl2019.util.Constants;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient {

    private static Retrofit retrofit = null;

    public static Retrofit getClient() {
        if (retrofit == null) {
            Gson gson = new GsonBuilder()
                    .setLenient()
                    .create();

//            retrofit = new Retrofit.Builder()
//                    .baseUrl(Constants.BASE_URL)
//                    .addConverterFactory(GsonConverterFactory.create(gson))
//                    .build();
        }
        return retrofit;
    }
}
