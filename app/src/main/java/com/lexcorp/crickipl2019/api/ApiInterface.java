package com.lexcorp.crickipl2019.api;

import com.lexcorp.crickipl2019.model.Auction;
import com.lexcorp.crickipl2019.util.Constants;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface ApiInterface {

    @GET(Constants.AUCTION_URL)
    Call<Auction> getAuction();
}
