package com.lexcorp.crickipl2019.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Auction extends GeneralModel {

    @Expose
    @SerializedName("results")
    public List<Results> results;

    public static class Results {
        @Expose
        @SerializedName("abbreviation")
        public String abbreviation;
        @Expose
        @SerializedName("base_price")
        public String basePrice;
        @Expose
        @SerializedName("id")
        public String id;
        @Expose
        @SerializedName("known_as")
        public String knownAs;
        @Expose
        @SerializedName("mtc")
        public String mtc;
        @Expose
        @SerializedName("overseas")
        public String overseas;
        @Expose
        @SerializedName("retained")
        public String retained;
        @Expose
        @SerializedName("role")
        public String role;
        @Expose
        @SerializedName("sold")
        public String sold;
        @Expose
        @SerializedName("sold_price")
        public String soldPrice;
        @Expose
        @SerializedName("team_id")
        public String teamId;
    }
}
