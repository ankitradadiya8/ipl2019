package com.lexcorp.crickipl2019.model;

public class MenuItem {

    public String title;

    public int resImage;

    public MenuItem(String title, int resImage) {
        this.title = title;
        this.resImage = resImage;
    }
}
