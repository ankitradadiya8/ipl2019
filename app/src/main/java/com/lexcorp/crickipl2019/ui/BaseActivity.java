package com.lexcorp.crickipl2019.ui;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.customtabs.CustomTabsIntent;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.lexcorp.crickipl2019.R;
import com.lexcorp.crickipl2019.api.ApiClient;
import com.lexcorp.crickipl2019.api.ApiInterface;
import com.lexcorp.crickipl2019.model.GeneralModel;
import com.lexcorp.crickipl2019.util.Constants;
import com.lexcorp.crickipl2019.util.Preferences;
import com.victor.loading.rotate.RotateLoading;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.TimeZone;

public class BaseActivity extends AppCompatActivity {

    private Dialog dialog;
    public InterstitialAd mInterstitialAd;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(getString(R.string.interstitial_ad_id));
    }

    public void showBannerAds(AdView mAdView) {
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
    }

    public void loadInterstitialAd() {
        mInterstitialAd.loadAd(new AdRequest.Builder().build());

        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                mInterstitialAd.loadAd(new AdRequest.Builder().build());
            }
        });
    }

    public void showInterstitialAd() {
        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public ApiInterface getService() {
        return ApiClient.getClient().create(ApiInterface.class);
    }

    public void failureError(String message) {
        hideProgress();
        if (!isEmpty(message))
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    public void validationError(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    public void toastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    public boolean checkStatus(GeneralModel model) {
        if (model == null)
            return false;

        if (model.success != null) {
            switch (model.success) {
                case "1":
                    return true;
            }
        } else if (model.flag == 1) {
            return true;
        } else if (model.flag == 0) {
            if (model.msg != null && model.msg.equalsIgnoreCase("Expired Token.")) {
                Preferences.clearPreferences(this);
                return false;
            }
        }
        failureError(model.msg);
        return false;
    }

    public boolean checkStatus(String response) {
        if (isEmpty(response))
            return false;

        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = null;
            if (jsonObject.has("msg")) {
                msg = jsonObject.getString("msg");
            }
            if (jsonObject.has("success")) {
                String success = jsonObject.getString("success");
                if (!isEmpty(success)) {
                    switch (success) {
                        case "0":
                            if (msg != null && !isEmpty(msg)) {
                                failureError(msg);
                            }
                            return false;
                        case "1":
                        case "1.0":
                            return true;
                    }
                }
            } else if (jsonObject.has("flag")) {
                String flag = jsonObject.getString("flag");
                if (flag.equals("1") || flag.equals("1.0")) {
                    return true;
                } else if (flag.equals("0") || flag.equals("0.0")) {
                    if (msg != null && msg.equalsIgnoreCase("Expired Token.")) {
                        Preferences.clearPreferences(this);
                        return false;
                    }
                }
            }
            failureError(msg);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return false;
    }

    public String getToken() {
        return Preferences.readString(this, Constants.FCM_TOKEN, "");
    }

    public String getDeviceType() {
        return "1"; // for Android
    }

    public String getTimeZone() {
        return TimeZone.getDefault().getID(); // for Android
    }

    public boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (cm != null && cm.getActiveNetworkInfo() != null) {
            return true;
        }
        Toast.makeText(this, "connect to internet", Toast.LENGTH_SHORT).show();
        return false;
    }

    public void gotoMainActivity(int screen) {
        Intent i = new Intent(this, MainActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
        finish();
        finishToRight();
    }

    public void clearTopActivity(Class<?> activityClass) {
        Intent i = new Intent(this, activityClass);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
        finish();
        finishToRight();
    }

    public void redirectActivity(Class<?> activityClass) {
        startActivity(new Intent(this, activityClass));
        openToLeft();
    }

    public void openToTop() {
        overridePendingTransition(R.anim.slide_in_up, R.anim.stay);
    }

    public void openToLeft() {
        overridePendingTransition(R.anim.trans_left_in, R.anim.trans_left_out);
    }

    public void finishToBottom() {
        overridePendingTransition(R.anim.stay, R.anim.slide_out_down);
    }

    public void finishToRight() {
        overridePendingTransition(R.anim.trans_right_in, R.anim.trans_right_out);
    }

    public void showProgress() {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }

        dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_loading);
        dialog.setCancelable(false);
        RotateLoading rotateLoading = dialog.findViewById(R.id.rotateloading);
        rotateLoading.start();
        dialog.show();
        WindowManager.LayoutParams lp = dialog.getWindow().getAttributes();
        lp.dimAmount = 0.0f;
        dialog.getWindow().setAttributes(lp);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }

    public void hideProgress() {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
    }

    public boolean isValidEmail(String target) {
        return (!isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }

    public boolean isValidMobile(String phone) {
        if (phone.contains("+"))
            phone = phone.replace("+", "");
        return (!isEmpty(phone) && Double.parseDouble(phone) > 0 && Patterns.PHONE.matcher(phone).matches() && phone.length() > 6);
    }

    public boolean isValidUrl(String url) {
        return (!isEmpty(url) && Patterns.WEB_URL.matcher(url.toLowerCase()).matches());
    }

    public boolean isEmpty(String s) {
        return TextUtils.isEmpty(s);
    }

    public void redirectUsingCustomTab(String url) {
        try {
            Uri uri = Uri.parse(url);
            CustomTabsIntent.Builder intentBuilder = new CustomTabsIntent.Builder();
            intentBuilder.setToolbarColor(ContextCompat.getColor(this, R.color.colorPrimary));
            intentBuilder.setSecondaryToolbarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
            CustomTabsIntent customTabsIntent = intentBuilder.build();
            customTabsIntent.launchUrl(this, uri);
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(this, "something went wrong", Toast.LENGTH_SHORT).show();
        }
    }

    public void shareApp() {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, "https://play.google.com/store/apps/details?id=" + getPackageName());
        sendIntent.setType("text/plain");
        startActivity(Intent.createChooser(sendIntent, "Share via..."));
    }
}
