package com.lexcorp.crickipl2019.ui;

import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.google.android.gms.ads.AdView;
import com.lexcorp.crickipl2019.R;
import com.lexcorp.crickipl2019.adapter.CategoryAdapter;
import com.lexcorp.crickipl2019.model.MenuItem;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends BaseActivity {

    @BindView(R.id.rv_menu)
    RecyclerView rvMenu;
    @BindView(R.id.adView)
    AdView adView;

    private ArrayList<MenuItem> arrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        getSupportActionBar().setTitle(getString(R.string.app_name));

        fillArray();
        setAdapter();
        showBannerAds(adView);
    }

    public void setAdapter() {
        rvMenu.setLayoutManager(new GridLayoutManager(this, 2));
        CategoryAdapter myAdapter = new CategoryAdapter(this, arrayList);
        rvMenu.setAdapter(myAdapter);
    }

    private void fillArray() {
        arrayList = new ArrayList<>();
        arrayList.add(new MenuItem("Schedule", R.drawable.sch));
        arrayList.add(new MenuItem("Teams", R.drawable.tms));
        arrayList.add(new MenuItem("Points & Caps", R.drawable.pts));
        arrayList.add(new MenuItem("News", R.drawable.nws));
        arrayList.add(new MenuItem("Live Scores", R.drawable.livescore));
        arrayList.add(new MenuItem("States", R.drawable.record));
    }
}
