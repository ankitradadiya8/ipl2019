package com.lexcorp.crickipl2019.ui;

import android.os.Bundle;
import android.support.design.widget.TabItem;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.google.android.gms.ads.AdView;
import com.lexcorp.crickipl2019.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PlayerInfoActivity extends BaseActivity {

    @BindView(R.id.container)
    ViewPager container;
    @BindView(R.id.adView)
    AdView adView;
    @BindView(R.id.adV)
    LinearLayout adV;
    @BindView(R.id.tabItem)
    TabItem tabItem;
    @BindView(R.id.tabItem2)
    TabItem tabItem2;
    @BindView(R.id.tabItem3)
    TabItem tabItem3;
    @BindView(R.id.tabs)
    TabLayout tabs;
    @BindView(R.id.progress_info_bar)
    ProgressBar progressInfoBar;
    @BindView(R.id.main_content)
    RelativeLayout mainContent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player_info);
        ButterKnife.bind(this);

        getSupportActionBar().setTitle(" - Players List");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }
}
