package com.lexcorp.crickipl2019.ui;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.google.android.gms.ads.AdView;
import com.google.gson.Gson;
import com.lexcorp.crickipl2019.R;
import com.lexcorp.crickipl2019.adapter.PlayerListAdapter;
import com.lexcorp.crickipl2019.model.Auction;
import com.lexcorp.crickipl2019.util.Constants;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PlayerListActivity extends BaseActivity {

    @BindView(R.id.rv_player_list)
    RecyclerView rvPlayerList;
    @BindView(R.id.adView)
    AdView adView;

    private String teamName;
    private ArrayList<Auction.Results> playerList;
    private PlayerListAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player_list);
        ButterKnife.bind(this);

        if (getIntent() != null && getIntent().getExtras() != null) {
            teamName = getIntent().getStringExtra(Constants.TEAM_NAME);
        }

        getSupportActionBar().setTitle(teamName + " - Players List");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        rvPlayerList.setLayoutManager(new LinearLayoutManager(this));

        playerList = new ArrayList<>();
        String teamsJson = loadAuction();
        if (teamName.equals("DC")) {
            teamName = "DD";
        }
        Auction auction = new Gson().fromJson(teamsJson, Auction.class);
        for (int i = 0; i < auction.results.size(); i++) {
            if (auction.results.get(i).abbreviation.equals(teamName)) {
                playerList.add(auction.results.get(i));
            }
        }

        if (teamName.equals("DD")) {
            teamName = "DC";
        }

        mAdapter = new PlayerListAdapter(this, playerList);
        rvPlayerList.setAdapter(mAdapter);
        showBannerAds(adView);
        loadInterstitialAd();
    }

    public String loadAuction() {
        String json = null;
        try {
            InputStream is = getAssets().open("auction.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    public void getAuction() {
        if (!isNetworkConnected())
            return;

        showProgress();

        Call<Auction> call = getService().getAuction();
        call.enqueue(new Callback<Auction>() {
            @Override
            public void onResponse(Call<Auction> call, Response<Auction> response) {
                Auction auction = response.body();
                if (auction != null) {
                    if (checkStatus(auction)) {
                        //languagesArray = (ArrayList<Language.Data>) language.data;
                    }
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<Auction> call, Throwable t) {
                failureError("get auction failed");
            }
        });
    }
}
