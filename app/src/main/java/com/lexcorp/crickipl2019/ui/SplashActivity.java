package com.lexcorp.crickipl2019.ui;

import android.os.Bundle;
import android.os.Handler;

import com.lexcorp.crickipl2019.R;

public class SplashActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                redirectActivity(MainActivity.class);
                finish();
            }
        }, 2000);
    }
}
