package com.lexcorp.crickipl2019.ui;

import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.google.android.gms.ads.AdView;
import com.lexcorp.crickipl2019.R;
import com.lexcorp.crickipl2019.adapter.TeamsAdapter;
import com.lexcorp.crickipl2019.model.MenuItem;
import com.lexcorp.crickipl2019.util.Constants;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TeamsActivity extends BaseActivity {

    @BindView(R.id.rv_menu)
    RecyclerView rvMenu;
    @BindView(R.id.adView)
    AdView adView;

    private ArrayList<MenuItem> arrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        getSupportActionBar().setTitle("IPL Teams");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        fillArray();
        setAdapter();

        showBannerAds(adView);
    }

    public void setAdapter() {
        rvMenu.setLayoutManager(new GridLayoutManager(this, 2));
        TeamsAdapter myAdapter = new TeamsAdapter(this, arrayList);
        rvMenu.setAdapter(myAdapter);
    }

    private void fillArray() {
        arrayList = new ArrayList<>();
        arrayList.add(new MenuItem(Constants.CSK, R.drawable.cskn));
        arrayList.add(new MenuItem(Constants.DD, R.drawable.ddn));
        arrayList.add(new MenuItem(Constants.KKR, R.drawable.kkrn));
        arrayList.add(new MenuItem(Constants.KXIP, R.drawable.k11n));
        arrayList.add(new MenuItem(Constants.MI, R.drawable.min));
        arrayList.add(new MenuItem(Constants.RCB, R.drawable.rcbn));
        arrayList.add(new MenuItem(Constants.RR, R.drawable.rrn));
        arrayList.add(new MenuItem(Constants.SRH, R.drawable.srhn));
    }
}
