package com.lexcorp.crickipl2019.ui;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.google.android.gms.ads.AdView;
import com.lexcorp.crickipl2019.R;
import com.lexcorp.crickipl2019.util.Constants;

import butterknife.BindView;
import butterknife.ButterKnife;

public class WebViewActivity extends BaseActivity {

    @BindView(R.id.webview)
    WebView webview;
    @BindView(R.id.adView)
    AdView adView;

    private String screenName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);
        ButterKnife.bind(this);

        if (getIntent() != null && getIntent().getExtras() != null) {
            screenName = getIntent().getStringExtra(Constants.SCREEN_NAME);
        }

        showBannerAds(adView);

        webview.getSettings().setJavaScriptEnabled(true);
        webview.setWebViewClient(new MyWebViewClient());

        showProgress();

        String url;
        switch (screenName) {
            case Constants.MATCHES:
                url = Constants.MATCH_URL;
                break;
            case Constants.NEWS:
                url = Constants.NEWS_URL;
                break;
            case Constants.TABLE:
                url = Constants.POINT_TABLE_URL;
                break;
            case Constants.STATES:
                url = Constants.STATES_URL;
                break;
            default:
                url = Constants.MATCH_URL;
                break;

        }
        webview.loadUrl(url);
    }

    private class MyWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            Log.e("URL", url);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            hideProgress();
        }
    }

    @Override
    public void onBackPressed() {
        if (webview.canGoBack()) {
            webview.goBack();
        } else {
            super.onBackPressed();
        }
    }
}
