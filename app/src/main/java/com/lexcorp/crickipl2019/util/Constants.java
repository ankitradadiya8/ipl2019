package com.lexcorp.crickipl2019.util;

public class Constants {

    public static final String BASE_URL = "http://crickexer.com/ipl2019/";
    public static final String AUCTION_URL = "auction.json";
    public static final String MATCH_URL = "https://www.google.com/search?q=ipl+schedule+2019&oq=ipl&aqs=chrome.0.69i59j0l2j69i61j69i60l2.887j0j7&sourceid=chrome&ie=UTF-8#sie=lg;/g/11f6593n2n;5;/m/03b_lm1;mt;fp;1;;";
    public static final String NEWS_URL = "https://www.google.com/search?q=ipl+schedule+2019&oq=ipl&aqs=chrome.0.69i59j0l2j69i61j69i60l2.887j0j7&sourceid=chrome&ie=UTF-8#sie=lg;/g/11f6593n2n;5;/m/03b_lm1;nw;fp;1;;";
    public static final String POINT_TABLE_URL = "https://www.google.com/search?q=ipl+schedule+2019&oq=ipl&aqs=chrome.0.69i59j0l2j69i61j69i60l2.887j0j7&sourceid=chrome&ie=UTF-8#sie=lg;/g/11f6593n2n;5;/m/03b_lm1;st;fp;1;;";
    public static final String STATES_URL = "https://www.google.com/search?q=ipl+schedule+2019&oq=ipl&aqs=chrome.0.69i59j0l2j69i61j69i60l2.887j0j7&sourceid=chrome&ie=UTF-8#sie=lg;/g/11f6593n2n;5;/m/03b_lm1;lb;fp;1;;";
    public static final String PLAYER_URL = "https://www.google.com/search?q=ipl+schedule+2019&oq=ipl&aqs=chrome.0.69i59j0l2j69i61j69i60l2.887j0j7&sourceid=chrome&ie=UTF-8#sie=lg;/g/11f6593n2n;5;/m/03b_lm1;pl;fp;1;;";

    public static final int DEVICE_TYPE = 2; // for android

    public static final String TEXT_REGULAR = "fonts/GoogleSans-Regular.ttf";
    public static final String TEXT_MEDIUM = "fonts/GoogleSans-Medium.ttf";

    public static final String FCM_TOKEN = "fcm_token";
    public static final String JWT = "JWT";
    public static final String SCREEN_NAME = "screenName";
    public static final String TEAM_NAME = "teamName";

    public static final String MATCHES = "matches";
    public static final String NEWS = "news";
    public static final String TABLE = "table";
    public static final String STATES = "states";
    public static final String PLAYERS = "players";

    public static final String CSK = "CSK";
    public static final String DD = "DD";
    public static final String KKR = "KKR";
    public static final String KXIP = "KXIP";
    public static final String MI = "MI";
    public static final String RCB = "RCB";
    public static final String RR = "RR";
    public static final String SRH = "SRH";

    public static final int NOTIFICATION_ID = 100;
    public static final int NOTIFICATION_ID_BIG_IMAGE = 101;
}
